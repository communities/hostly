# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from . import hotel_reservation_beds24
from . import hotel_room_type
from . import hotel_room
from . import res_partner
