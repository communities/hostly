# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
import datetime
import urllib2
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.tools import misc, DEFAULT_SERVER_DATETIME_FORMAT
from odoo import models, fields, api, _
from decimal import Decimal

class HotelRoomType(models.Model):

    _inherit = "hotel.room.type"

    inv_code = fields.Char('Channel Room Type Code', size=64, required=True)