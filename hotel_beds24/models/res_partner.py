# -*- coding: utf-8 -*-
from datetime import datetime, date
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models

class ResPartnerHotelBeds24(models.Model):

    _inherit = "res.partner"

    c_email = fields.Char('Channel email')
    
    #_sql_constraints = [
    #    ('c_email_ref_uniq', 'unique (c_email)', 'O email deve ser unico!'),
    #]
