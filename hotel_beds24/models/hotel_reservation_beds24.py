# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
import datetime
from datetime import datetime as dtime
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt
from odoo.exceptions import except_orm, ValidationError
import pytz
#######################################
import json
import requests

api_url_base = "https://www.beds24.com/api/json/"
########################################

class HotelConfigBeds24(models.Model):

    _name = "hotel.config.beds24"
    
    name = fields.Char('Name', size=64)
    apiKey = fields.Char('API Key')
    propKey = fields.Char('Property Key')
    propId = fields.Char('Property ID')
    active = fields.Boolean('Activada')

class HotelReservationUpdates(models.Model):

    _name = "hotel.reservation.updates"
    
    bookid = fields.Char('bookid')
    status = fields.Char('status')

class HotelReservationBeds24(models.Model):

    _inherit = "hotel.reservation"
    
    message = fields.Text('Extra Information')
    notes = fields.Text('Notes')
    apireference = fields.Char('Channel Code', size=64, readonly=True)
    total_guest = fields.Integer('Total Guest', compute='_guests', size=64, readonly=True)
    nights = fields.Integer('Total Nigths', compute='_nights', size=64, readonly=True)
    relation_codes = fields.Char('Channel Codes')
    sync = fields.Selection([('yes', 'Yes'), ('no', 'No'),
                              ('cancel', 'Cancel')],
                             'Sync', readonly=True,
                             default=lambda *a: 'no')

    @api.depends('adults', 'children')
    def _guests(self):
        for record in self:
            if record.adults:
                record.total_guest = record.adults + record.children
            else:
                record.nights = 0

    @api.depends('checkin', 'checkout')
    def _nights(self):
        for record in self:
            if record.checkin:
                record.nights = relativedelta(
                    fields.Date.from_string(record.checkout),
                    fields.Date.from_string(record.checkin)).days
            else:
                record.nights = 0

    	
##################### Cancelar reservas online

    def cancel_bookings(self):
        
        keys = self.env['hotel.config.beds24'].search([('active','=', True)])
        
        api_token = {
            "authentication": {
                "apiKey": keys.apiKey,
                "propKey": keys.propKey
            },
            "bookId": " ",
            "roomId": " ",
            "unitId": " ",
            "roomQty": "1",
            "status": "0",
            "notifyUrl": "false",
        }
        book_ids = self.relation_codes
        book_ids = str(book_ids).split()
        
        i = 0
        contador = False
        for value in self.reservation_line:

            if contador == False:
                api_token['bookId'] = self.channel_code
                api_token['roomId'] = value.categ_id.inv_code
                api_token['unitId'] = value.reserve.unit_code
                contador = True
            else:
                api_token['bookId'] = book_ids[i]
                api_token['roomId'] = value.categ_id.inv_code
                api_token['unitId'] = value.reserve.unit_code
                i = i + 1 
                       
        
            api_url = '{0}setBooking'.format(api_url_base)
    
            response = requests.post(api_url, json=api_token)

    
            if response.status_code == 200:
                print (json.loads(response.content.decode('utf-8'))) # Produces a dictionary out of the given string 
                self.sync = 'cancel'
        

    @api.one
    def cancel_reservation(self):
        if not self.apireference:
            self.cancel_bookings()
        return super(HotelReservationBeds24, self).cancel_reservation()
        
####################UPDATE############################ 

    def update_bookings(self):
    	
        keys = self.env['hotel.config.beds24'].search([('active','=', True)])
        
        checkin_date = self.checkin
        checkin_date = parse(checkin_date)
        checkin_date = checkin_date.strftime("%Y-%m-%d")
        
        checkout_date = self.checkout
        checkout_date = parse(checkout_date)
        checkout_date = checkout_date - datetime.timedelta(days=1)#-1 dia
        checkout_date = checkout_date.strftime("%Y-%m-%d")
    
        api_token = {
            "authentication": {
                "apiKey": keys.apiKey,
                "propKey": keys.propKey
            },
            "bookId": " ",
            "roomId": " ",
            "unitId": " ",
            "roomQty": "1",
            "status": "1",
            "firstNight": checkin_date,
            "lastNight": checkout_date,
            "numAdult": self.adults,
            "numChild": self.children,
            "notifyUrl": "false",
        }
       
        book_ids = self.relation_codes
        book_ids = book_ids.split()
        
        i = 0
        contador = False
        for value in self.reservation_line:

            if contador == False:
                api_token['bookId'] = self.channel_code
                api_token['roomId'] = value.categ_id.inv_code
                api_token['unitId'] = value.reserve.unit_code
                contador = True
            else:
                api_token['bookId'] = book_ids[i]
                api_token['roomId'] = value.categ_id.inv_code
                api_token['unitId'] = value.reserve.unit_code
                i = i + 1 

        
            api_url = '{0}setBooking'.format(api_url_base)
    
            response = requests.post(api_url, json=api_token)
    
            if response.status_code == 200:
                #print (json.loads(response.content.decode('utf-8'))) # Produces a dictionary out of the given string
                #print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                self.sync = 'yes'


########################  Criar Reserva Online  ###########################

    def set_booking(self):
    	
        keys = self.env['hotel.config.beds24'].search([('active','=', True)])
        
        checkin_date = self.checkin
        checkin_date = parse(checkin_date)
        checkin_date = checkin_date.strftime("%Y-%m-%d")
        
        checkout_date = self.checkout
        checkout_date = parse(checkout_date)
        checkout_date = checkout_date - datetime.timedelta(days=1)#-1 dia
        checkout_date = checkout_date.strftime("%Y-%m-%d")
    
        api_token = {
            "authentication": {
                "apiKey": keys.apiKey,
                "propKey": keys.propKey
            },
            "roomId": " ",
            "unitId": " ",
            "roomQty": "1",
            "status": "1",
            "firstNight": checkin_date,
            "lastNight": checkout_date,
            "numAdult": self.adults,
            "numChild": self.children,
            "guestTitle": " ",
            "guestFirstName": self.partner_id.name,
            "guestName": " ",
            "guestEmail": self.partner_id.email,
            "guestPhone": self.partner_id.phone,
            "guestMobile": self.partner_id.mobile,
            "guestFax": self.partner_id.fax,
            "guestAddress": self.partner_id.street,
            "guestCity": self.partner_id.city,
            "guestPostcode": self.partner_id.zip,
            "guestCountry": self.partner_id.country_id.name,
            "guestArrivalTime": " ",
            "guestVoucher": " ",
            "guestComments": " ",
            "guestCardType": " ",
            "guestCardNumber": self.vcard_number,
            "guestCardName": self.vcard_name,
            "guestCardExpiry": "01\/17",
            "guestCardCVV": self.vcard_cvv,
            "message": self.message,
            "custom1": "text",
            "custom2": "text",
            "custom3": "text",
            "custom4": "text",
            "custom5": "text",
            "custom6": "text",
            "custom7": "text",
            "custom8": "text",
            "custom9": "text",
            "custom10": "text",
            "notes": self.notes,
            "flagColor": "ff0000",
            "flagText": "Show booking in red",
            "price": "100.00",
            "deposit": "10.00",
            "tax": "5.00",
            "commission": "15.00",
            "refererEditable": "online",
            "notifyUrl": "false",
            "notifyGuest": "false",
            "notifyHost": "false",
            "assignBooking": "false",
            "invoice": [
                {
                    "description": "lodging",
                    "status": "",
                    "qty": "1",
                    "price": "123.45",
                    "vatRate": "10",
                    "type": "0"
                }
            ],
            "infoItems": [
                {
                    "code": "PAYMENT",
                    "text": "Paid $100"
                }
            ]
            }
        codigos = ""
        contador = False
        for value in self.reservation_line:
            api_token['roomId'] = value.categ_id.inv_code
            api_token['unitId'] = value.reserve.unit_code
        
            api_url = '{0}setBooking'.format(api_url_base)
    
            response = requests.post(api_url, json=api_token)
    
            if response.status_code == 200:
                print (json.loads(response.content.decode('utf-8')))
                x = json.loads(response.content.decode('utf-8'))
                
                self.sync = 'yes'

                if contador == True:
                    codigos = codigos + " " + str(x['bookId'])
                    
                else:
                    self.channel_code =  str(x['bookId'])      	    
                
                contador = True	
        
        self.relation_codes = codigos 
