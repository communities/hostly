# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel Beds24',
    'version': '1.0',
    'author': 'Communities - Comunicações, Lda',
    'summary': 'Quick actions and views, etc.',
    'category': 'Extra Tools',
    'description':
    """
Hotel Beds24
==============

        """,
    'data': [
        'security/ir.model.access.csv',
        'views/beds24_views.xml',
    ],
    'depends': ['hotel_reservation'],
    'auto_install': True,
}
