# -*- coding: utf-8 -*-
import werkzeug
import datetime
import json
import requests
import math
import base64
import logging
from dateutil.parser import parse
_logger = logging.getLogger(__name__)
requests.packages.urllib3.disable_warnings()

from odoo import http, _
from odoo.http import Response
from odoo.exceptions import AccessError
from odoo.http import request

########################
# A push notification will be sent to this URL when a new booking, booking modification or cancellation occurs.
# The bookid and status are appended to your url like this example.
# https://yoururl.com/yourpage?bookid=12345678&status=new
# Possible values for status are: new, modify, cancel 
########################

api_url_base = "https://www.beds24.com/api/json/"

class BookingUpdateController(http.Controller):

    @http.route('/bkupdate', type="http", auth="public", website=True, csrf=False)
    def bkupdate_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value

        existing_record = http.request.env['hotel.reservation'].sudo().search([('channel_code','=', values['bookid'])])
        
        if values['status'] == 'cancel':
            update_booking = existing_record.sudo().write({
            'state': 'cancel'
            })
            print ('################## reserva cancelada')

            request.env['hotel.reservation.updates'].sudo().create({'bookid' : values['bookid'], 'status' : values['status'] })
        
        if existing_record.channel_code and values['status'] == 'modify':
            print ('################## alterar reserva')
            
            last_update = request.env['hotel.reservation.updates'].sudo().search([])[-1].create_date
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            print ('################## Last update #######################')
            print (last_update)
            
            api_token = {
                "authentication": {
                    "apiKey": keys.apiKey,
                    "propKey": keys.propKey
                },
                "bookId": values['bookid'],
                "modifiedSince": last_update
            }

            api_url = '{0}getBookings'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                #x = json.dumps(json.loads(response.content.decode('utf-8')))


                x1 = json.loads(response.content.decode('utf-8'))
                
                n = len(x1) - 1
                while n != -1:
                    x = dict(x1[n])
                    print(x['bookId'])
                    n = n - 1


                    ##### Formatacao das datas
                    c_checkin = parse(x['firstNight'])
                    c_checkout = parse(x['lastNight'])
                    c_checkout = c_checkout + datetime.timedelta(days=1)#+1 dia 
                
                    nights = c_checkout - c_checkin #fazer sempre antes do !strftime!
                    nights = nights.days
                    print('nr de noites')
                    print nights
                    
                    c_checkin = c_checkin + datetime.timedelta(hours=14)# checkin min as 14 
                    c_checkout = c_checkout + datetime.timedelta(hours=12)# checkout as 12
                    
                    if c_checkin < datetime.datetime.now():
                        c_checkin = datetime.datetime.now()
                        c_checkin = c_checkin.strftime("%Y-%m-%d %H:%M:%S")
                        print('check in atrasado')
                        print(c_checkin)
         
                    partner = http.request.env['res.partner'].sudo().search([('c_email', '=', x['guestEmail'])])
                    existing_record.sudo().write({
                    'partner_id' : partner.id,
                    'partner_order_id' : partner.id,
                    'partner_invoice_id' : partner.id,
                    'partner_shipping_id' : partner.id,
                    'checkin' : c_checkin, ##############firstNight
                    'checkout' : c_checkout, #########lastNight
                    'children' : x['numChild'],
                    #'reservation_line.categ_id' : 3,
                    'channel_price' : x['price'],
                    'channel' : x['refererEditable'], ############
                    'channel_code' : x['bookId'],
                    'apireference' : x['apiReference'],
                    #'state' : state, ######################### status mudar    
                    'message' : x['guestComments'],
                    'notes' : x['notes'],
                    'nights' : nights,
                    'total_guest' : int(x['numAdult'])+int(x['numChild']),
                    'adults' : x['numAdult']
                     })
                 
                    categ = http.request.env['hotel.room.type'].sudo().search([('inv_code', '=', x['roomId'])])[0]
                    room = http.request.env['hotel.room'].sudo().search([('categ_id', '=', categ.id ), ('unit_code', '=', x['unitId'])])[0]
                            
                    reserv_line = http.request.env['hotel_reservation.line'].sudo().search([('line_id', '=', existing_record.id)])
                    if reserv_line:
                        reserv_line.sudo().write({
                            'categ_id' : categ.id, 
                            'reserve': [(6, False, [room.id])]
                            })   
                         
                request.env['hotel.reservation.updates'].sudo().create({'bookid' : values['bookid'], 'status' : values['status'] })
            
        if not existing_record.channel_code and values['status'] == 'new':         
            print ('################## reserva não existe #######################')
            
            last_update = request.env['hotel.reservation.updates'].sudo().search([])[-1].create_date
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            print ('################## Last update #######################')
            print (last_update)
            
            api_token = {
                "authentication": {
                    "apiKey": keys.apiKey,
                    "propKey": keys.propKey
                },
                "bookId": values['bookid'],
                "modifiedSince": last_update
            }

            api_url = '{0}getBookings'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                #x = json.dumps(json.loads(response.content.decode('utf-8')))
                x1 = json.loads(response.content.decode('utf-8'))
                
                n = len(x1) - 1
                while n != -1:
                    x = dict(x1[n])
                    print(x['bookId'])
                    n = n - 1

                ##############################  

                ##### Formatacao das datas
                    c_checkin = parse(x['firstNight'])
                    c_checkout = parse(x['lastNight'])
                
                    c_checkout = c_checkout + datetime.timedelta(days=1)#+1 dia 
                
                    nights = c_checkout - c_checkin #fazer sempre antes do !strftime!
                    nights = nights.days
                    print('nr de noites')
                    print nights
                
                    c_checkin = c_checkin + datetime.timedelta(hours=14)# checkin min as 14  
                    c_checkout = c_checkout + datetime.timedelta(hours=12)# checkout as 12
             
                    if c_checkin < datetime.datetime.now():
                        c_checkin = datetime.datetime.now()
                        c_checkin = c_checkin.strftime("%Y-%m-%d %H:%M:%S")
                        print('check in atrasado')
                        print(c_checkin)
 
                    guest_name = x['guestFirstName'] + ' ' + x['guestName']
                
                #status = state ######## ********** mudar

                    partner = http.request.env['res.partner'].sudo().search([('c_email', '=', x['guestEmail'])])
                    if partner:
                        print('############ O paceiro ja existe - cria a reserva ###########################')
                        r = request.env['hotel.reservation'].sudo().create({
                            'partner_id' : partner.id,
                            'partner_order_id' : partner.id,
                            'partner_invoice_id' : partner.id,
                            'partner_shipping_id' : partner.id,
                            'checkin' : c_checkin,
                            'checkout' : c_checkout,
                            'children' : x['numChild'],
                            #'reservation_line.categ_id' : 3,
                            'channel_price' : x['price'],############## mudar
                            'channel' : x['refererEditable'],############
                            'channel_code' : x['bookId'],
                            'apireference' : x['apiReference'],
                            #'state' : state,   
                            'message' : x['guestComments'],
                            'notes' : x['notes'],
                            'nights' : nights,
                            'total_guest' : int(x['numAdult'])+int(x['numChild']),
                            'adults' : x['numAdult']
                            })
                     
                        categ = http.request.env['hotel.room.type'].sudo().search([('inv_code', '=', x['roomId'])])[0]
                        room = http.request.env['hotel.room'].sudo().search([('categ_id', '=', categ.id ), ('unit_code', '=', x['unitId'])])[0]
                        
                        #request.env['hotel_reservation.line'].sudo().create({'line_id' : r.id, 'categ_id' : categ.id })    
                        request.env['hotel_reservation.line'].sudo().create({'line_id' : r.id, 'categ_id' : categ.id, 'reserve': [(4, room.id)] })
                 
                    if not partner:
                        new_partner = request.env['res.partner'].sudo().create({
                            'name': guest_name, 
                            'street': x['guestAddress'],  
                            'city': x['guestCity'], 
                            'zip': x['guestPostcode'], 
                            #'country': x['guestAddress'],
                            'phone': x['guestPhone'], 
                            'mobile': x['guestMobile'], 
                            'email': x['guestEmail'], 
                            'c_email': x['guestEmail'] 
                            })

                        r = request.env['hotel.reservation'].sudo().create({
                            'partner_id' : new_partner.id,
                            'partner_order_id' : new_partner.id,
                            'partner_invoice_id' : new_partner.id,
                            'partner_shipping_id' : new_partner.id,
                            'checkin' : c_checkin, ##############firstNight
                            'checkout' : c_checkout, #########lastNight
                            'children' : x['numChild'],
                            #'reservation_line.categ_id' : 3,
                            'channel_price' : x['price'],
                            'channel' : x['refererEditable'], ############
                            'channel_code' : x['bookId'],
                            'apireference' : x['apiReference'],
                            #'state' : state, ######################### status mudar    
                            'message' : x['guestComments'],
                            'notes' : x['notes'],
                            'nights' : nights,
                            'total_guest' : int(x['numAdult'])+int(x['numChild']),
                            'adults' : x['numAdult']
                            })
                 
                        categ = http.request.env['hotel.room.type'].sudo().search([('inv_code', '=', x['roomId'])])[0]
                        room = http.request.env['hotel.room'].sudo().search([('categ_id', '=', categ.id ), ('unit_code', '=', x['unitId'])])[0]
                        
                        request.env['hotel_reservation.line'].sudo().create({'line_id' : r.id, 'categ_id' : categ.id, 'reserve': [(4, room.id)] })
                     
                    request.env['hotel.reservation.updates'].sudo().create({'bookid' : values['bookid'], 'status' : values['status'] })

#####################################

                #return werkzeug.utils.redirect("/web")
