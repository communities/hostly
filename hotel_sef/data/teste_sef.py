import requests

# Set the name of the XML file.
xml_file = "template.xml"

headers = {'Content-Type':'text/xml'}

# Open the XML file.
with open(xml_file) as xml:
    # Give the object representing the XML file to requests.post.
    r = requests.post('http://www.sef.pt/bawsdev/boletinsalojamento.asmx?wsdl.', data=xml)

print (r.content);