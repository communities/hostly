# -*- coding: utf-8 -*-

from odoo import api, fields, models
#from odoo.addons.website.models.website import slug


                                         
class HotelICAO(models.Model):
    _name = 'hotel.icao'
    
    name = fields.Char(string='País')
    code = fields.Char(size=3, string='Código')


class HotelForeignGuests(models.Model):
    _name = 'hotel.foreign.guests'
    _description = 'Reservations Guests'

    name = fields.Char(size=40, string='Nome', help="Este campo pode não ser preenchido se o nome completo do hóspede só tiver um nome. Neste caso esse nome deverá estar no campo Apelido.")
    surname = fields.Char(size=40, string='Apelido', required=True, help="Apelido do hóspede")
    nationality_id = fields.Many2one('hotel.icao', string='Nacionalidade', required=True, help="Código de Nacionalidade - Tabela ICAO")
    birthplace = fields.Char(size=40, string='Local de Nascimento', help="Local de Nascimento. Este campo é opcional")
    birth_date = fields.Date(string='Data Nascimento', required=True, help="Data de Nascimento – anterior à data do dia. Quando o documento de identificação do hóspede não mencionar o mês e o dia de nascimento, deve ser assumido o dia 1 de Janeiro.– Formato AAAAMMDD.")
    document_number = fields.Char(size=16, string='Número Documento', required=True, help="Número do documento de identificação sem espaços embebidos")
    document_type = fields.Selection([('B','Bilhete de Identidade'), ('P','Passaporte'), ('O','Outro documento de identificação')], string="Tipo Documento", required=True)
    issuing_country = fields.Many2one('hotel.icao', string='País Emissor', required=True, help="Código de País - Tabela ICAO")
    residence_country = fields.Many2one('hotel.icao', string='País Residência', required=True, help="Código de País - Tabela ICAO")
    residence_place = fields.Char(size=30, string='Local Residência', help="Local habitual de residência. Este campo é opcional.")
    reserve_id = fields.Many2one('hotel.reservation', 'Reservation',
                                      copy=False)
                                      
class HotelReserevation(models.Model):
    _inherit = "hotel.reservation"

    guests = fields.One2many('hotel.foreign.guests', 'reserve_id', 'Guests')
                                     
