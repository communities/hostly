# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel SIBA-TEC',
    'version': '1.0',
    'author': 'Communities - Comunicações, Lda',
    'summary': 'Quick actions and views, etc.',
    'category': 'Extra Tools',
    'description':
    """
Hotel SIBA-TEC
==============

        """,
    'data': [
        'security/ir.model.access.csv',
        'views/foreign_guests.xml',
        'data/hotel.icao.csv'
    ],
    'depends': ['hotel_reservation', 'hotel'],
    'auto_install': True,
}
