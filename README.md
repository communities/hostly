# hostly
Gestão Hoteleira simples e intuitiva

- Gestão centralizada de preços, reservas, disponibilidades, perfis de clientes e entidades, check-in e check-out.
- Sistema de faturação certificado pela Autoridade Tributária.
- Notificações automáticas.
- Dashboards e relatórios específicos.
- Interligação com canais de reservas online.
- Comunicações automáticas com o Banco de Portugal e de estadia de estrangeiros ao SEF.(Não disponível de momento)
- Faturação certificada pela Autoridade Tributária (geração SAFT-PT).