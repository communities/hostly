odoo.define('hotel_dashboard', function (require) {
"use strict";

var core = require('web.core');
var Widget = require('web.Widget');
var Model = require('web.Model');
var session = require('web.session');
var framework = require('web.framework');
var webclient = require('web.web_client');

var QWeb = core.qweb;
var _t = core._t;

var Dashboard = Widget.extend({
    template: 'DashboardHotel',

    init: function(parent, data){
        this.all_dashboards = ['reservations', 'checkintoday', 'checkouttoday', 'currentgueststoday', 'hostly_share', 'hostly_chart'];
        return this._super.apply(this, arguments);
    },

    start: function(){
        return this.load(this.all_dashboards);
    },

    load: function(dashboards){
        var self = this;
        var loading_done = new $.Deferred();
        session.rpc("/hotel_dashboard/data", {}).then(function (data) {
            // Load each dashboard
            var all_dashboards_defs = [];
            _.each(dashboards, function(dashboard) {
                var dashboard_def = self['load_' + dashboard](data);
                if (dashboard_def) {
                    all_dashboards_defs.push(dashboard_def);
                }
            });

            // Resolve loading_done when all dashboards defs are resolved
            $.when.apply($, all_dashboards_defs).then(function() {
                loading_done.resolve();
            });
        });
        return loading_done;
    },

    load_reservations: function(data){
        return  new DashboardHotelReservation(this, data.reservations).replace(this.$('.o_hotel_dashboard_reservations'));
    },

    load_checkintoday: function(data){
        return new DashboardHotelCheckintoday(this, data.checkintoday).replace(this.$('.o_hotel_dashboard_checkintoday'));
    },

    load_checkouttoday: function(data){
        return new DashboardHotelCheckouttoday(this, data.checkouttoday).replace(this.$('.o_hotel_dashboard_checkouttoday'));
    },

    load_currentgueststoday: function(data){
        return new DashboardHotelCurrentgueststoday(this, data.currentgueststoday).replace(this.$('.o_hotel_dashboard_currentgueststoday'));
    },

    load_hostly_share: function(data){
        return new DashboardHotelShare(this, data.hostly_share).replace(this.$('.o_hotel_dashboard_share'));
    },

    load_hostly_chart: function(data){
        return new DashboardHotelChart(this, data.hostly_chart).replace(this.$('.o_hotel_dashboard_chart'));
    },  
});




var DashboardHotelReservation = Widget.extend({

    template: 'DashboardHotelReservation',

    events: {
        'click .o_browse_reservation': 'on_new_reservation',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_new_reservation: function(){
        this.do_action('hotel_reservation.action_hotel_reservation_dash_all');
    },
});


var DashboardHotelCheckintoday = Widget.extend({

    template: 'DashboardHotelCheckintoday',

    events: {
        'click .o_browse_ckin': 'on_ckin_process',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_ckin_process: function(){
        this.do_action('hotel_reservation.action_hotel_reservation_dash_ckin');
    },
});


var DashboardHotelCheckouttoday = Widget.extend({

    template: 'DashboardHotelCheckouttoday',

    events: {
        'click .o_browse_ckout': 'on_ckout_process',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_ckout_process: function(){
        this.do_action('hotel_reservation.action_hotel_reservation_dash_ckout');
    },
});


var DashboardHotelCurrentgueststoday = Widget.extend({

    template: 'DashboardHotelCurrentgueststoday',

    events: {
        'click .o_browse_cguests': 'on_cguests_process',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        return this._super.apply(this, arguments);
    },

    on_cguests_process: function(){
        this.do_action('hotel.open_hotel_folio1_form_tree_current');
    },
});



////////////////////////////////////////////

var DashboardHotelChart = Widget.extend({
    template: 'DashboardHotelChart',


});

////////////////////////////////////////////

var DashboardHotelShare = Widget.extend({
    template: 'DashboardHotelShare',

    events: {
        'click .tw_share': 'share_twitter',
        'click .fb_share': 'share_facebook',
        'click .li_share': 'share_linkedin',
    },

    init: function(parent, data){
        this.data = data;
        this.parent = parent;
        this.share_url = 'https://www.hostly.pt';
        this.share_text = encodeURIComponent("hostly - Central Reservation Office");
    },

    share_twitter: function(){
        var popup_url = _.str.sprintf( 'https://twitter.com/intent/tweet?tw_p=tweetbutton&text=%s %s',this.share_text,this.share_url);
        this.sharer(popup_url);
    },

    share_facebook: function(){
        var popup_url = _.str.sprintf('https://www.facebook.com/sharer/sharer.php?u=%s', encodeURIComponent(this.share_url));
        this.sharer(popup_url);
    },

    share_linkedin: function(){
        var popup_url = _.str.sprintf('http://www.linkedin.com/shareArticle?mini=true&url=%s&title=hostly Software&summary=%s&source=www.hostly.pt', encodeURIComponent(this.share_url), this.share_text);
        this.sharer(popup_url);
    },

    sharer: function(popup_url){
        window.open(
            popup_url,
            'Share Dialog',
            'width=600,height=400'); // We have to add a size otherwise the window pops in a new tab
    }
});

core.action_registry.add('hotel_dashboard.main', Dashboard);

return {
    Dashboard: Dashboard,
    DashboardHotelShare: DashboardHotelShare,
};

});
