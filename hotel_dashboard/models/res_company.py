# -*- coding: utf-8 -*-
from openerp import api, fields, models

class ResHotelrunnerLink(models.Model):

    _inherit = "res.company"

    hotelrunner_link = fields.Char(string="Hotelrunner partner link")
