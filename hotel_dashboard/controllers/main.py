# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta

from odoo import fields, http
from odoo.exceptions import AccessError
from odoo.http import request
from odoo import release

from collections import defaultdict

class WebSettingsHotelDashboard(http.Controller):

    @http.route('/hotel_dashboard/data', type='json', auth='user')
    def hotel_dashboard_data(self, **kw):
        if not request.env.user.has_group('hotel.group_hotel_user'):
            raise AccessError("Access Denied")
            
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        hotel_reservation = request.env['hotel.reservation'].search_count([
            ('checkin', '>=', now)
        ])
        
        hotel_reservation_draft = request.env['hotel.reservation'].search_count([
            ('checkin', '>=', now),
            ('state', '=', 'draft')
        ])
        
        hotel_reservation_cancel = request.env['hotel.reservation'].search_count([
            ('checkin', '>=', now),
            ('state', '=', 'cancel')
        ])
        
        hotel_reservation_confirm = request.env['hotel.reservation'].search_count([
            ('checkin', '>=', now),
            ('state', '=', 'confirm')
        ])
        
        if hotel_reservation > 0:
            reservation_draft_percent = hotel_reservation_draft * 100 / hotel_reservation
            reservation_cancel_percent = hotel_reservation_cancel * 100 / hotel_reservation
            reservation_confirm_percent = hotel_reservation_confirm * 100 / hotel_reservation
        
        else:
           reservation_draft_percent = 0
           reservation_cancel_percent = 0
           reservation_confirm_percent = 0    	

        hotel_reservations = []
        h_reservations = request.env['hotel.reservation'].sudo().search([('checkin','>=', now), ('state', 'in', ['confirm','draft','done'])])
        counts = defaultdict(int)
        for i in h_reservations:
            counts[i.checkin[:10]] += 1
            hotel_reservations = dict(counts)

        tnow = datetime.now()
        today = tnow.strftime('%Y-%m-%d')
        nweek = tnow + timedelta(days=5)#-1 dia
        nextweek = nweek.strftime('%Y-%m-%d') 

        todaystart = datetime.now().strftime('%Y-%m-%d 00:00:00')
        todayend = datetime.now().strftime('%Y-%m-%d 23:59:59') 
               
        todayckin = datetime.now().strftime('%Y-%m-%d 14:00:00')
        todayckut = datetime.now().strftime('%Y-%m-%d 12:00:00')
        todaynow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        
        now = datetime.now()
        tomorrow = now + timedelta(days=1)#-1 dia
        tomorrow = tomorrow.strftime('%Y-%m-%d 12:00:00')

        ckintoday = []
        ckin = request.env['hotel.reservation'].sudo().search([('checkin','>=', todaystart), ('checkin', '<=', todayend), ('state', 'in', ['confirm','draft','done'])])
        for i in ckin:
            r = i.reservation_line[0]
            rr = r.reserve[0]
            ckintd = {'guest': i.partner_id.name, 
            'nights': i.nights,
            'rooms': rr.name,
            'channel': i.channel}
            ckintoday.append(ckintd)
       
        #print ckintoday
        ckin_count = len(ckin)
        
        ckouttoday = []
        ckout = request.env['hotel.reservation'].sudo().search([('checkout','>=', todaystart), ('checkout', '<=', todayend), ('state', 'in', ['confirm','draft','done'])])
        for i in ckout:
            r = i.reservation_line[0]
            rr = r.reserve[0]
            ckouttd = {'guest': i.partner_id.name, 
            'nights': i.nights, 
            'rooms': rr.name,
            'channel_price': i.channel_price}
            ckouttoday.append(ckouttd)
        
        #print ckouttoday
        ckout_count = len(ckout) 
        
        currentguests = []
        cguests = request.env['hotel.folio'].sudo().search([('checkin_date','<=', todaynow), ('checkout_date', '>=', todaynow)])
        for i in cguests:
            r = i.reservation_id.reservation_line[0]
            rr = r.reserve[0]
            guests = i.reservation_id.adults + i.reservation_id.children
            cgueststd = {'guest': i.reservation_id.partner_id.name,
            'guests': guests, 
            'rooms': rr.name,
            'checkout': i.reservation_id.checkout}
            currentguests.append(cgueststd)

            
        #print current guests
        cguests_count = len(cguests)
        #cguests_count = map(sum, zip(guests))
        
        return {
            'reservations': {
                'hotel_reservation': hotel_reservation,
                'reservation_draft_percent': reservation_draft_percent,
                'reservation_cancel_percent': reservation_cancel_percent,
                'reservation_confirm_percent': reservation_confirm_percent,
                
                'hotel_reservations': hotel_reservations,
                'today': today,
                'nextweek': nextweek,
            },
            'checkintoday': {
                'ckintoday': ckintoday,
                'ckin_count': ckin_count,
            },
            'checkouttoday': {
                'ckouttoday': ckouttoday,
                'ckout_count': ckout_count,
            },
            'currentgueststoday': {
                'currentguests': currentguests,
                'cguests_count': cguests_count,
            },
            'hostly_share': {
                'server_version': release.version,
                'debug': request.debug,
            }
        }
