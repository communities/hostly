# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel Dashboard',
    'version': '1.0',
    'author': 'Communities - Comunicações, Lda',
    'summary': 'Quick actions and views, etc.',
    'category': 'Extra Tools',
    'description':
    """
Hotel dashboard

        """,
    'data': [
        'views/dashboard_views.xml',
        'views/dashboard_templates.xml',
        #'views/res_company_views.xml',
    ],
    'depends': ['web_planner','hotel','hotel_reservation'],
    'qweb': ['static/src/xml/dashboard.xml'],
    'auto_install': True,
}
