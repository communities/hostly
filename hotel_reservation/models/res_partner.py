# -*- coding: utf-8 -*-
from datetime import datetime, date
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models

class ResPartnerHotelReservation(models.Model):

    _inherit = "res.partner"

    doc_type = fields.Selection([('cc','Cartão de Cidadão'), ('passport','Passaporte'), ('bi','Bilhete de Identidade')], string="Documento de Identificação")
    doc_number = fields.Char('Numero do Documento')
    doc_date = fields.Date('Data de Validade')
    birthday = fields.Date('Date of Birth')
