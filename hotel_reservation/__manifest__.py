# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel Reservation Management',
    'version': '10.0.1.0.0',
    'author': 'Communities - Comunicações, Odoo Community Association (OCA), Serpent Consulting \
                Services Pvt. Ltd., Odoo S.A.',
    'category': 'Generic Modules/Hotel Reservation',
    'website': 'http://www.hostly.pt',
    'depends': ['hotel', 'web_timeline', 'product', 'stock', 'mail'],
    'license': 'AGPL-3',
    'demo': [
        'views/hotel_reservation_data.xml',
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/hotel_reservation_wizard.xml',
        'wizard/sale_view_sale_advance_payment_inv.xml',
        'report/hotel_reservation_report.xml',
        'views/hotel_reservation_sequence.xml',
        'views/hotel_reservation_view.xml',
        'data/hotel_scheduler.xml',
        'views/email_temp_view.xml',
        'views/report_checkin.xml',
        'views/report_checkout.xml',
        'views/max_room.xml',
        'views/room_res.xml',
        'views/room_summ_view.xml',
    ],
    'js': ['static/src/js/hotel_room_summary.js', ],
    'qweb': ['static/src/xml/hotel_room_summary.xml'],
    'css': ['static/src/css/room_summary.css'],
    'images': ['static/description/HotelReservation.png'],
    'installable': True,
    'auto_install': False,
}
