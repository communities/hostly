# -*- coding: utf-8 -*-
import werkzeug
import datetime
import json
import requests
import math
import base64
import logging
from dateutil.parser import parse
_logger = logging.getLogger(__name__)
requests.packages.urllib3.disable_warnings()

from odoo import http, _
from odoo.http import Response
from odoo.exceptions import AccessError
from odoo.http import request

api_url_base = "https://www.beds24.com/api/json/"

class hotelbooking(http.Controller):
#------------------------------------------------------
    @http.route('/page/homepage', type="http", auth="public", website=True)
    def hotel_home(self, **kwargs):
        rooms = http.request.env['hotel.room.type'].sudo().search([('website_published','=',True)], limit=3)
        
        return http.request.render('website.homepage', {'rooms': rooms} )
#------------------------------------------------------

    @http.route('/booking', type='http', auth="public", website=True, csrf=False)
    def booking_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        rooms = request.env['hotel.room.type'].sudo().search([('website_published','=',True)])

        
        return http.request.render('website_hotel_demo.booking', {'rooms': rooms} )

###########################################################################################################

    @http.route('/booking/process', type='http', auth="public", website=True, csrf=False)
    def booking_search_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        
        checkin = parse(values['checkin'])
        checkin = checkin.strftime("%Y-%m-%d")
        
        checkout = parse(values['checkout'])
        checkout = checkout.strftime("%Y-%m-%d")

        if values['checkin'] and values['checkout']:
            print ('################## Efectua Consulta')
            
            print (checkin)
            
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            api_token = {
                "checkIn": checkin,
                "checkOut": checkout,
                "propId": keys.propId,
                "numAdult": values['adults'],
                "numChild": values['children']
            }

            api_url = '{0}getAvailabilities'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                #data = json.dumps(json.loads(response.content.decode('utf-8')))
                data = json.loads(response.content.decode('utf-8'))
                
                checkin_r = parse(values['checkin'])
                checkin_r = checkin_r.strftime("%d %b, %Y")
                checkout_r = parse(values['checkout'])
                checkout_r = checkout_r.strftime("%d %b, %Y")
                numAdult = values['adults']
                if values['children'] >= 1:
                    numChild = values['children']
                else:               
                    numChild = '0'
                    
                #print data["propId"]

                roomid = []
                available = []
                for k, v in data.items():
                    if isinstance(v, dict):
                        if v['roomsavail'] != '0' and v.get('price'):
                        	available.append(v)
                        	roomid.append(k)
        
        print ('quartos disponiveis')
        print available                	
        print ('quartos da pesquisa')                	
        print roomid
        
        rooms = request.env['hotel.room.type'].sudo().search([('website_published','=',True), ('inv_code', 'in', roomid)])
        print ('quartos locais apos pesquisa')
        print rooms            
        countries = request.env['res.country'].sudo().search([])

        return http.request.render('website_hotel_demo.booking', {'countries': countries, 
        'rooms': rooms,
        'croom': available, 
        'checkIn': checkin_r, 
        'checkOut': checkout_r, 
        "numAdult": numAdult, 
        "numChild": numChild })
        
###########################################################################################################
#
#
#
###########################################################################################################

    @http.route('/booking/confirm', type='http', auth="public", website=True)
    def booking_confirm(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        
        firstnight = parse(values['checkin'])
        firstnight = firstnight.strftime("%Y-%m-%d")
        
        lastnight = parse(values['checkout'])
        lastnight = lastnight - datetime.timedelta(days=1)#-1 dia
        lastnight = lastnight.strftime("%Y-%m-%d")
        
        guestcardexpiry = values['guestcardexpiry_month'] + '/' + values['guestcardexpiry_year']

        if values['checkin'] and values['checkout']:
            print ('################## Prepara o envio da reserva')
            
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            api_token = {
                "authentication": {
				        "apiKey": keys.apiKey,
				        "propKey": keys.propKey
				    },
				    "roomId": values['roomid'],
				    "unitId": "1",
				    "roomQty": "1",
				    "status": "1",
				    "firstNight": firstnight,
				    "lastNight": lastnight,
				    "numAdult": values['adults'],
				    "numChild": values['children'],
				    "guestTitle": " ",
				    "guestFirstName": values['guestfirstname'],
				    "guestName": values['guestname'],
				    "guestEmail": values['guestemail'],
				    "guestPhone": values['guestphone'],
				    "guestMobile": values['guestphone'],
				    "guestFax": " ",
				    "guestAddress": values['guestaddress'],
				    "guestCity": values['guestcity'],
				    "guestPostcode": values['guestpostcode'],
				    "guestCountry": values['guestcountry'],
				    "guestArrivalTime": " ",
				    "guestVoucher": " ",
				    "guestComments": " ",
				    "guestCardType": values['guestcardtype'],
				    "guestCardNumber": values['guestcardnumber'],
				    "guestCardName": values['guestcardname'],
				    "guestCardExpiry": guestcardexpiry,
				    "guestCardCVV": values['guestcardcvv'],
				    "message": values['message'],
				    "custom1": "text",
				    "custom2": "text",
				    "custom3": "text",
				    "custom4": "text",
				    "custom5": "text",
				    "custom6": "text",
				    "custom7": "text",
				    "custom8": "text",
				    "custom9": "text",
				    "custom10": "text",
				    "notes": "VIP",
				    "flagColor": "ff0000",
				    "flagText": "Show booking in red",
				    "price": values['ptotal'],
				    "deposit": " ",
				    "tax": " ",
				    "commission": " ",
				    "refererEditable": "online",
				    "notifyUrl": "true",
				    "notifyGuest": "true",
				    "notifyHost": "false",
				    "assignBooking": "false",
				    "invoice": [
				        {
				            "description": " ",
				            "status": "",
				            "qty": " ",
				            "price": " ",
				            "vatRate": " ",
				            "type": " "
				        }
				    ],
				    "infoItems": [
				        {
				            "code": "PAYMENT",
				            "text": " "
				        }
				    ]
				}

            api_url = '{0}setBooking'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print ('############################################')
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                
                check_in = values['checkin']
                check_out = values['checkout']
                adults = values['adults']
                children = values['children']
                p_total = values['ptotal']
                message = values['message']
                guest_name = values['guestfirstname'] + ' ' + values['guestname']
                guest_address = values['guestaddress']
                guest_postcode = values['guestpostcode']
                guest_city = values['guestcity']
                guest_country = values['guestcountry']
                
                return http.request.render('website_hotel_demo.booking_confirm', {'guest_name': guest_name, 
                'check_in': check_in, 
                'check_out': check_out, 
                'adults': adults, 
                'children': children, 
                'p_total': p_total, 
                'message': message,
                'guest_address': guest_address, 
                'guest_postcode': guest_postcode, 
                'guest_city': guest_city, 
                'guest_country': guest_country })
        
###########################################################################################################

    @http.route('/experiences', type='http', auth="public", website=True)
    def hotel_experiences(self, **kwargs):
        
        return http.request.render('website_hotel_demo.experiences')
        
###########################################################################################################

    @http.route('/teste', type='http', auth="public", website=True)
    def hotel_teste(self, **kwargs):
        
        return http.request.render('website_hotel_demo.booking_confirm')
        
###########################################################################################################

    @http.route('/room/<id>', type="http", auth="public", website=True)
    def hotel_room(self, id, **kwargs):
        room = http.request.env['hotel.room.type'].sudo().browse(int(id))
        rooms = http.request.env['hotel.room.type'].sudo().search([('website_published','=',True), ('id', '!=', int(room.id))])
        room_images = request.env['hotel.room.image'].sudo(). \
            search([('image_id', '=', int(room.id))])
        
        rooms_count = len(rooms)
            
        return http.request.render('website_hotel_demo.room', {'room': room, 'rooms': rooms, 'room_images': room_images, 'rooms_count': rooms_count} )
        