# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name':'Website Hotel Oliveira',
    'category': 'Theme',
    'website': 'https://www.communities.pt',
    'summary': 'Hotel',
    'version':'1.0',
    'description': """
Online reservation system and booking software
==========================
        """,
    'author':'Communities',
    'data': [
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/room_type.xml',
        'views/banner_views.xml',
        'views/hotel_template.xml',
        'views/homepage_template.xml',
        'views/booking-system.xml',
        #'views/experiences.xml',
        'views/contact_template.xml',
        'views/aboutus_template.xml',
        'views/near_template.xml',
        'views/policy_template.xml',
        'views/404_template.xml',
        'views/login_template.xml',
        'views/room_template.xml',
        'views/facility_template.xml',
        #'data/hotel.room.facility.csv',
        #'data/website.menu.csv'
    ],
    'depends': ['website', 'crm', 'website_crm', 'hotel', 'hotel_reservation', 'website_portal', 'web_widget_timepicker', 'hci_website_instagram'],
}
