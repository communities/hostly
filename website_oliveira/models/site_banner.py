# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SiteImage(models.Model):
    _name = 'website.image'
    _description = 'Website Image'

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    btn_text = fields.Char(string='Button Text')
    btn_link = fields.Char(string='button Link')
    image_alt = fields.Text(string='Image Label')
    image = fields.Binary(string='Image')
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='Image URL')
    website_banner_id = fields.Many2one('website.banner', string="Website",
                                      copy=False)
class SiteBanner(models.Model):
    _name = 'website.banner'
    
    name = fields.Char(string='Name')
    images = fields.One2many('website.image', 'website_banner_id', 'Images')