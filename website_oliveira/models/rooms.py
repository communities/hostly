# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.website.models.website import slug

class RoomBed(models.Model):
    _name = 'hotel.room.bed'
    _description = 'Type of bed'
    
    name = fields.Selection(
        [('Cama individual', 'Cama individual'), ('Cama individual extragrande', 'Cama individual extragrande'),
         ('Cama de casal', 'Cama de casal'), ('Cama queen-size', 'Cama queen-size'), ('Cama king-size', 'Cama king-size')],
        'Camas', default='Cama de casal', track_visibility='onchange')
    bed_num = fields.Integer(string='Quantidade')
    room_type_id = fields.Many2one('hotel.room.type', string='Room Type Reference',
        ondelete='cascade', index=True)

class RoomFacility(models.Model):
    _name = 'hotel.room.facility'
    _description = 'Most popular facilities'
    
    name = fields.Char(string='Name')
    css_code = fields.Char(string='Glyphs css code')
                                         
class RoomImage(models.Model):
    _name = 'hotel.room.image'
    _description = 'Room Image'

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    image_alt = fields.Text(string='Image Label')
    image = fields.Binary(string='Image')
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='Image URL')
    image_id = fields.Many2one('hotel.room.type', 'Room',
                                      copy=False)
                                      
class WebHotelRoomType(models.Model):
    _inherit = "hotel.room.type"

    website_published = fields.Boolean('Active', default=True)
    webprice = fields.Float('Public Price')
    ck_in_start = fields.Float('Check-in Start')
    ck_in_end = fields.Float('Check-in End')
    ck_out_start = fields.Float('Check-out Start')
    ck_out_end = fields.Float('Check-out End')
    webintro = fields.Char('Breve Descrição', size=200,)
    condictions = fields.Text('Cancellation/prepayment')
    webinfo = fields.Html('Room overview')
    webinfo_2 = fields.Html('Additional info')
    webinfo_3 = fields.Html('Children and extra beds')
    pets = fields.Selection([('Permitidos', 'Permitidos'), ('Não Permitidos', 'Não Permitidos')], 'Pets', default='Não Permitidos')
    images = fields.One2many('hotel.room.image', 'image_id', 'Images')
    facilities_ids = fields.Many2many('hotel.room.facility', 'hotel_room_facility_rel', 'src_id', 'dest_id', string='Room facilities')
    beds_ids = fields.One2many('hotel.room.bed', 'room_type_id', string='Beds')
                                     
