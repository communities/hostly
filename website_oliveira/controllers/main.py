# -*- coding: utf-8 -*-
import werkzeug
import datetime
import json
import requests
import math
import base64
import logging
import re
from dateutil.parser import parse
_logger = logging.getLogger(__name__)
requests.packages.urllib3.disable_warnings()

from odoo import http, _
from odoo.http import Response
from odoo.exceptions import AccessError
from odoo.http import request


api_url_base = "https://www.beds24.com/api/json/"

class hotelbooking(http.Controller):
#------------------------------------------------------
    @http.route('/page/homepage', type="http", auth="public", website=True)
    def hotel_home(self, **kwargs):
        rooms = http.request.env['hotel.room.type'].sudo().search([('website_published','=',True)], limit=3)
        banners = http.request.env['website.banner'].sudo().search([], limit=1)
        
        return http.request.render('website.homepage', {'rooms': rooms, 'banners': banners} )
#------------------------------------------------------

    @http.route('/booking', type='http', auth="public", website=True, csrf=False)
    def booking_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        rooms = request.env['hotel.room.type'].sudo().search([('website_published','=',True)])
        rooms_count = len(rooms)

        return http.request.render('website_oliveira.booking', {'rooms': rooms, 'rooms_count': rooms_count} )

###########################################################################################################

    @http.route('/booking/process', type='http', auth="public", website=True, csrf=False)
    def booking_search_process(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        checkin_str =  re.sub(r"<.+?>", "", values['checkin'])
        checkout_str = re.sub(r"<.+?>", "", values['checkout'])
        
        print (values['checkin'])
        print (checkin_str)
        print ('----------------------------------------')
        
        
        #checkin = parse(values['checkin'])
        checkin = parse(checkin_str)
        checkin = checkin.strftime("%Y-%m-%d")
        
        #checkout = parse(values['checkout'])
        checkout = parse(checkout_str)
        checkout = checkout.strftime("%Y-%m-%d")
        
        room_inv_code = values['inv_code']

        if values['checkin'] and values['checkout']:
        
            print ('################## Efectua Consulta')
            
            print (checkin)
            
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            api_token = {
                "checkIn": checkin,
                "checkOut": checkout,
                "propId": keys.propId,
                "roomId": room_inv_code,
                "numAdult": values['adults'],
                "numChild": values['children']
            }

            api_url = '{0}getAvailabilities'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                #data = json.dumps(json.loads(response.content.decode('utf-8')))
                data = json.loads(response.content.decode('utf-8'))
                
                #checkin_r = parse(values['checkin'])
                checkin_r = parse(checkin_str)
                checkin_r = checkin_r.strftime("%d %b, %Y")
                #checkout_r = parse(values['checkout'])
                checkout_r = parse(checkout_str)
                checkout_r = checkout_r.strftime("%d %b, %Y")
                numAdult = values['adults']
                if values['children'] >= 1:
                    numChild = values['children']
                else:               
                    numChild = '0'
                    
                #print data["propId"]

                roomid = []
                available = []
                for k, v in data.items():
                    if isinstance(v, dict):
                        if v['roomsavail'] != '0' and v.get('price'):
                        	available.append(v)
                        	roomid.append(k)
        
        print ('quartos disponiveis')
        print available                	
        print ('quartos da pesquisa')                	
        print roomid
        
        rooms = request.env['hotel.room.type'].sudo().search([('website_published','=',True), ('inv_code', 'in', roomid)])
        rooms_count = len(rooms)
        print ('quartos locais apos pesquisa')
        print rooms   
        print rooms_count        
        countries = request.env['res.country'].sudo().search([])

        return http.request.render('website_oliveira.booking', {'countries': countries, 
        'rooms': rooms,
        'rooms_count': rooms_count,
        'croom': available, 
        'checkIn': checkin_r, 
        'checkOut': checkout_r, 
        "numAdult": numAdult, 
        "numChild": numChild })
        
###########################################################################################################

    @http.route('/booking/checkout', type='http', auth="public", website=True)
    def booking_checkout(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        room_name = values['room_name']
        checkin = parse(values['checkin'])
        checkin = checkin.strftime("%d %B %Y")
        
        checkout = parse(values['checkout'])
        checkout = checkout.strftime("%d %B %Y")
        
        room_inv_code = values['inv_code']
        numAdult = values['numAdult']
        numChild = values['numChild']
        price = values['price']
        priceFormated = values['priceFormated']
	    
        
        countries = request.env['res.country'].sudo().search([])
        partner = request.env.user.partner_id

        return http.request.render('website_oliveira.booking_checkout', {'countries': countries,  
        'partner': partner,
        'room_name': room_name,
        'checkIn': checkin, 
        'checkOut': checkout, 
        'roomid': room_inv_code, 
        "numAdult": numAdult, 
        "numChild": numChild,
        'price': price,
        'priceFormated': priceFormated })
    	
    	
###########################################################################################################

    @http.route('/booking/confirm', type='http', auth="public", website=True)
    def booking_confirm(self, **kwargs):

        values = {}
	for field_name, field_value in kwargs.items():
	    values[field_name] = field_value
	    
        
        firstnight = parse(values['checkin'])
        firstnight = firstnight.strftime("%Y-%m-%d")
        
        lastnight = parse(values['checkout'])
        lastnight = lastnight - datetime.timedelta(days=1)#-1 dia
        lastnight = lastnight.strftime("%Y-%m-%d")
        
        guestcardexpiry = values['guestcardexpiry_month'] + '/' + values['guestcardexpiry_year']

        if values['checkin'] and values['checkout']:
            print ('################## Prepara o envio da reserva')
            
            keys = request.env['hotel.config.beds24'].sudo().search([('active','=', True)])
            
            api_token = {
                "authentication": {
				        "apiKey": keys.apiKey,
				        "propKey": keys.propKey
				    },
				    "roomId": values['roomid'],
				    "unitId": "1",
				    "roomQty": "1",
				    "status": "1",
				    "firstNight": firstnight,
				    "lastNight": lastnight,
				    "numAdult": values['numAdult'],
				    "numChild": values['numChild'],
				    "guestTitle": " ",
				    "guestFirstName": values['guestfirstname'],
				    "guestName": values['guestname'],
				    "guestEmail": values['guestemail'],
				    "guestPhone": values['guestphone'],
				    "guestMobile": values['guestphone'],
				    "guestFax": " ",
				    "guestAddress": values['guestaddress'],
				    "guestCity": values['guestcity'],
				    "guestPostcode": values['guestpostcode'],
				    "guestCountry": values['guestcountry'],
				    "guestArrivalTime": " ",
				    "guestVoucher": " ",
				    "guestComments": " ",
				    "guestCardType": values['guestcardtype'],
				    "guestCardNumber": values['guestcardnumber'],
				    "guestCardName": values['guestcardname'],
				    "guestCardExpiry": guestcardexpiry,
				    "guestCardCVV": values['guestcardcvv'],
				    "message": values['message'],
				    "custom1": "text",
				    "custom2": "text",
				    "custom3": "text",
				    "custom4": "text",
				    "custom5": "text",
				    "custom6": "text",
				    "custom7": "text",
				    "custom8": "text",
				    "custom9": "text",
				    "custom10": "text",
				    "notes": "VIP",
				    "flagColor": "ff0000",
				    "flagText": "Show booking in red",
				    "price": values['ptotal'],
				    "deposit": " ",
				    "tax": " ",
				    "commission": " ",
				    "refererEditable": "online",
				    "notifyUrl": "true",
				    "notifyGuest": "true",
				    "notifyHost": "false",
				    "assignBooking": "false",
				    "invoice": [
				        {
				            "description": values['room_name'],
				            "status": " ",
				            "qty": "1",
				            "price": values['ptotal'],
				            "vatRate": "6.00",
				            "type": " "
				        }
				    ],
				    "infoItems": [
				        {
				            "code": "PAYMENT",
				            "text": " "
				        }
				    ]
				}

            api_url = '{0}setBooking'.format(api_url_base)

            response = requests.post(api_url, json=api_token)
    
            print (response)

            if response.status_code == 200:
                #print json.loads(response.content.decode('utf-8')) # Produces a dictionary out of the given string
                print ('############################################')
                print (json.dumps(json.loads(response.content.decode('utf-8')))) # 'dumps' gets the dict from 'loads' this time
                data = json.loads(response.content.decode('utf-8'))


                print ('Resposta')
                print data["bookId"]  
                print data["success"]


                
                check_in = values['checkin']
                check_out = values['checkout']
                adults = values['numAdult']
                children = values['numChild']
                p_total = values['ptotal']
                message = values['message']
                guest_name = values['guestfirstname'] + ' ' + values['guestname']
                guest_address = values['guestaddress']
                guest_postcode = values['guestpostcode']
                guest_city = values['guestcity']
                guest_country = values['guestcountry']
                guest_email = values['guestemail']
                guest_phone = values['guestphone']
                room_name = values['room_name']
                guest_card_number = values['guestcardnumber']
                bookId = data["bookId"]
                success = data["success"]
                
                return http.request.render('website_oliveira.booking_confirm', {'guest_name': guest_name, 
                'check_in': check_in, 
                'check_out': check_out, 
				    'guest_email': guest_email,
				    'guest_phone': guest_phone,
                'adults': adults, 
                'children': children, 
                'p_total': p_total, 
                'room_name': room_name,
                'message': message,
                'guest_address': guest_address, 
                'guest_postcode': guest_postcode, 
                'guest_city': guest_city, 
                'guest_country': guest_country,
                'guest_card_number': guest_card_number,
                'bookId': bookId,
                'success': success })
        
###########################################################################################################

    @http.route('/facility', type='http', auth="public", website=True)
    def hotel_facility(self, **kwargs):
        
        return http.request.render('website_oliveira.facility')
        
###########################################################################################################

    @http.route('/privacy_policy', type='http', auth="public", website=True)
    def hotel_policy(self, **kwargs):
        
        return http.request.render('website_oliveira.privacy_policy')
        
###########################################################################################################

    @http.route('/near', type='http', auth="public", website=True)
    def hotel_near(self, **kwargs):
        
        return http.request.render('website_oliveira.near')
        
###########################################################################################################

    @http.route('/room/<id>', type="http", auth="public", website=True)
    def hotel_room(self, id, **kwargs):
        room = http.request.env['hotel.room.type'].sudo().browse(int(id))
        #rooms = http.request.env['hotel.room.type'].sudo().search([('website_published','=',True), ('id', '!=', int(room.id))])
        #room_images = request.env['hotel.room.image'].sudo(). \
        #    search([('image_id', '=', int(room.id))])
        
        #rooms_count = len(rooms)
            
        #return http.request.render('website_oliveira.room', {'room': room, 'rooms': rooms, 'room_images': room_images, 'rooms_count': rooms_count} )
        return http.request.render('website_oliveira.room', {'room': room} )
        