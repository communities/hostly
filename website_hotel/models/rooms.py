# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.website.models.website import slug


class WebHotelRoomType(models.Model):
    _inherit = "hotel.room.type"

    website_published = fields.Boolean('Active', default=True)
    webprice = fields.Float('Public Price')
    webintro = fields.Char('Breve Descrição', size=200,)
    webinfo = fields.Text('Descrição estendida para website')
    images = fields.One2many('hotel.room.image', 'image_id', 'Images')
    facilities_ids = fields.Many2many('hotel.room.facility', 'hotel_room_facility_rel', 'src_id', 'dest_id', string='Most popular facilities')
                                     
class RoomFacility(models.Model):
    _name = 'hotel.room.facility'
    _description = 'Most popular facilities'
    
    name = fields.Char(string='Name')
    css_code = fields.Char(string='Glyphs css code')
                                         
class RoomImage(models.Model):
    _name = 'hotel.room.image'
    _description = 'Room Image'

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    image_alt = fields.Text(string='Image Label')
    image = fields.Binary(string='Image')
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='Image URL')
    image_id = fields.Many2one('hotel.room.type', 'Room',
                                      copy=False)